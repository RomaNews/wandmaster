﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour {

    // Objektum, amire a kamera néz
    public Transform lookAt;

    private Vector3 offset;

    private float distance = 5.0f;
    private float yOffSet = 3.5f;

    private void Start()
    {
        offset = new Vector3(0, yOffSet,  -1f * distance);

    }

    private void Update()
    {
        transform.position = lookAt.position + offset;
    }
}
