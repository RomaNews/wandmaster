﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class VirtualJoystick : MonoBehaviour, IDragHandler, IPointerUpHandler, IPointerDownHandler {

    // Kép és Vektorváltozók

    private Image bgImg;
    private Image joystickImg;
    private Vector3 inputvector;

    private void Start()
    {
        // Változókba menti az Unityben létrehozott képeket

        bgImg = GetComponent<Image>();
        joystickImg = transform.GetChild(0).GetComponent<Image>();

    }

   

    public virtual void OnDrag(PointerEventData ped)
    {
        Vector2 pos;

        // Vizsgálja, hogy a Joystickon belül van-e azu érintés

        if (RectTransformUtility.ScreenPointToLocalPointInRectangle(bgImg.rectTransform, ped.position, ped.pressEventCamera, out pos))
        {
            pos.x = (pos.x / bgImg.rectTransform.sizeDelta.x);
            pos.y = (pos.y / bgImg.rectTransform.sizeDelta.y);

            inputvector = new Vector3(pos.x * 2 + 1, 0, pos.y * 2 - 1);
            inputvector = (inputvector.magnitude > 1.0f) ? inputvector.normalized : inputvector;

            // A joystick mozgatása

            joystickImg.rectTransform.anchoredPosition = new Vector3(inputvector.x * (bgImg.rectTransform.sizeDelta.x / 3),
                                                                     inputvector.z * (bgImg.rectTransform.sizeDelta.y / 3));

            Debug.Log(inputvector);
        }

    }

    // Érintés kezdete

    public virtual void OnPointerDown(PointerEventData ped)
    {
        OnDrag(ped);
    }

    //Érintés vége

    public virtual void OnPointerUp(PointerEventData ped)
    {
        inputvector = Vector3.zero;
        joystickImg.rectTransform.anchoredPosition = Vector3.zero;


    }

    // Milyen a joystick mozgatásának iránya

    public float Horizontal()
    {
        if (inputvector.x != 0)
            return inputvector.x;
        else
            return Input.GetAxis("Horizontal");
    }

    public float Vertical()
    {
        if (inputvector.z != 0)
            return inputvector.z;
        else
            return Input.GetAxis("Vertical");
    }

}



