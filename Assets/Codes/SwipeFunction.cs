﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwipeFunction : MonoBehaviour
{

    //változók a swipe paramétereihez

    public float maxTime;
    public float minSwipeDist;

    float startTime;
    float endTime;

    Vector3 startPos;
    Vector3 endPos;

    float swipeDistance;
    float swipeTime;



    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);

            if (touch.phase == TouchPhase.Began)
            {

                startTime = Time.time;
                startPos = touch.position;

            }
            else if (touch.phase == TouchPhase.Ended)
            {
                endTime = Time.time;
                endPos = touch.position;

                swipeDistance = (endPos - startPos).magnitude;
                swipeTime = (endTime - startTime);

                if (swipeTime < maxTime && swipeDistance > minSwipeDist)
                {
                    swipe();
                }

            }

        }

    }
    //swipe függyvény, felismeri a swipe irányátát, amely később a varázslatokhoz kellenek

    void swipe()
    {
        Vector2 distance = endPos - startPos;

        if (Mathf.Abs(distance.x) > Mathf.Abs(distance.y))
        {
            Debug.Log("Horizontális szfájp");
            if (distance.x > 0)
            {
                Debug.Log("Jobbra húzás");
            }
            if (distance.x < 0)
            {
                Debug.Log("Balra húzás");
            }


        }


    }
}